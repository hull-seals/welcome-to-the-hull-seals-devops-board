# Changelog
All notable changes to Seal services will be documented in this file. Each individual project may have their own releases, and this log may include some information that does not appear in our Git repositories.

Standalone features not reliant on other systemic changes (such as website tools) may be implemented before the "official" version release dates.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.6.1] - 2022-08-13
A continuation cleanup run, designed to continue the improvements from the last release.

### Added:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added the ability for the bot to link up with a YOURLS URL shortener. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a number of new logging entries. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a new EDSM-linked exception for more detailed error logging and stability. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a new test condition to validate handling of incomplete EDSM responses. (@Rixxan)

### Changed:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Replaced all uses of python `lower()` with python `casefold()` (Thanks, @theunkn0wn1). (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated documentation for new-file standards. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Changed a number of imports to fix duplicate import paths. (@Rixxan)

### Removed:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a number of copyright year slugs that did nothing but cause problems. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a comment that served no purpose anymore. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a few instances of unreachable code. (@Rixxan)

## [1.6] - 2022-07-17
A cleanup run on all Seal systems focusing mostly on stability and reliability.

### Added:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a number of Pytest-based tests. (@Rixxan, @Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added encoding markers to a number of filesystem commands. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added proper attribution to exceptions. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a number of new logging entries. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a number of new docstrings and code comments. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added loguru for logging system duties. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Adds cattr structures for user info fetching. (@theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added missing update to fallback in listsupport. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a context manager for notification timer. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a number of missing `await` statements. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Enabled compresison support for aiohttp. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added EDSM Mocking for Tests. (@Rixxan, @theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added bot mocking for tests. (@Rixxan, @theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a warning to README for users on Linux. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added the ability to use custom shutdown messages. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added EDDB Dump File reader for diversion stations. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a Diversion command to find the 5 closest systems to a given EDSM location. (@Rixxan, @theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a handler and logger for Pylde-unknown IRC commands. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Added a tooltip to explain O2 Synthesis (@Rixxan)

### Changed:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Several formatting updates. (@Rixxan, @theunkn0wn1, @Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Changed how exceptions are logged and presented. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Converted a number of if-true-else statements to bools. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Renamed a number of improperly-named functions. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Re-ordered a number of imports to comply with conventions. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue with an improperly-labeled exception. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Changed the attribution of a system exit command. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a number of imports to more specific values. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Improved logic of a number of tests. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Renamed a number of improperly-named variables. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Recategorized a number of improperly leveled errors. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated requirements to more recent versions. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a number of spelling and grammar mistakes. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated the copyright date on numerous files. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Various logic improvements. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Changed how variables are interpolated into logging entries. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a few "FIXME" and "TODO"s. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a bug with how lengths of arguments were processed. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a bug that would occur due to unintentional redefining of inbuilt functions. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a crash that could occur if the bot was launched in Offline mode. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixes a bug that could cause a regression in future releases of Pydle. (@theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixes an incorrect exception instantiation. (@theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixes a bug due to an incorrect argument in the partchannel command. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixes a bug due to a mutable default variable. (@theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updates various documentation files. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated how Discord webhook configuration options are stored. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Refactored the notification formatting system. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Unified dataclass method with attrs. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Changed EDSM API coding to improve forward-compatibility. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Moved test fixtures to their own subdirectory. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue with logging system where tasks were not properly defined. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Improve and class-ify Landmarks and Carriers system. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a bug that could occur on --new cache overrides with EDSM. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a bug that could occur if a system existed, but was outside of Landmark coverage. (@Rixxan)
- [CMDR Management](https://gitlab.com/hull-seals/code/website-subsections/cmdr-management/): Updated Variable Names for CMDR Management. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Fixed an issue where incomplete case data on Code Blacks would result in a failed notification. (@Rixxan)

### Removed:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed some extraneous exception handlers. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed some in-line string breaks. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a number of useless else statements. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a number of useless f-strings. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a number of useless pass statements. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a number of unused variables. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a number of useless keys lookups from dicts. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed Grafana logging system. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed some duplicate functions. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed some duplicate tests. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removes an unnecessary pool. (@theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed some unnecessary dependencies. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a number of useless imports. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a number of obsolete ancillary files. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a number of unnecessary imports. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a few Global variables. (@Rixxan)

## [1.5.3] - 2022-03-17
A bugfix for HalpyBOT, to improve a number of systems and fix issues with the EDSM module.

### Added:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a function to replace duplicate coordinate finding fragments. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added some more logging keys to EDSM module. (@theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added attrs and cattrs to the project requirements. (@theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Add expected EDSM JSON API response to test files. (@theunkn0wn1)

### Changed:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Completely reworked the EDSM linkup to be more resilient and efficient. (@theunkn0wn1)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Refactored /src/ to /halpybot/. (@theunkn0wn1, @Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an improperly formatted regex string. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Upgraded a number of required libraries to the latest versions. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Changed a number of function and variable names to conform with PEP8 Standards. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a few documentation links. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a number of inconsistent whitespace issues. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a few arguments that equal their defaults. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated the request filtering to allow a number of valid responses. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Renamed attribute titles for the landmark json file. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Apply a number of spacing or style changes. (@theunkn0wn1)

### Removed:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed some improperly set asyncio marks. (@theunkn0wn1, @Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a redundant requirement entry. (@theunkn0wn1)

## [1.5.2] - 2022-02-26
A bugfix for HalpyBOT, to fix a number of issues introduced with 1.5.

### Added:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added the Git version to the main server GET route if running HalpyBOT as a Git Repo (Recommended). (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added command argument validation to Part command. (@Rixxan)

### Changed:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated the default configuration example file. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated the default UserAgent string. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated included Help json file to include more administrative commands. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated Help command logic to allow for multiple command queries. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a number of spelling and grammar mistakes in the documentation files. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where negative numbers or decimals would break the Coords command. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Corrected a problem with Twitter strings revealing too much information. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated documentation instructions for installing HalpyBOT. (@Rixxan)

### Removed:
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a few unused, out-of-date debug files. (@Rixxan)

## [1.5.1] - 2022-02-11
A bugfix for HalpyBOT.

### Added
n/a

### Changed
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a problem where the Twitter module would not properly function. (@Rixxan, @Rik079).

### Removed
n/a

## [1.5] - 2022-02-11
A major content addition to HalpyBOT, focusing on quality of life and new feature development.

### Added
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Enabled an improved Brotli-based compression method for the main web server. (@Rixxan)
- [Seal Template](https://gitlab.com/hull-seals/code/seals-template): Added Global Site Theming System. (@Rixxan)
- [Dispatch Tools](https://gitlab.com/hull-seals/code/website-subsections/dispatcher-tools): Added Case Edit functionality to most case details. (@Rixxan, @KennyRosser, Jager)
- [Dispatch Tools](https://gitlab.com/hull-seals/code/website-subsections/dispatcher-tools): Added Date and Time of Paperwork Filing to Case Edit (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Add a basic notice handler. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added ability to disable the Twitter module. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added packaged Backup Fact Updater module. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added packaged DSSA Location Updater module. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added contextual help options to most commands. (@Stuntphish, @Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a system to create a Log file and folder if it does not exist. (@Stuntphish)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added support for requests sent with either LF or CRLF. (@Stuntphish, @Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a method to check given system names for misspellings or completeness in EDSM. (@Stuntphish)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a custom UserAgent check for HalpyBOT so people know who is poking their servers. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added timestamps to log output. (@Stuntphish)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a dedicated "RRJoin" Command. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added an additional alias for the shutdown command. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added additional aliases to the CyberSignal command. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added timestamp to halpybot status reporting. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added offline mode status to halpybot status reporting. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a feature where if Landmark systems fail, the nearest DSSA carrier is found. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a warning if non-registered users are assigned to a case with the !go fact/command. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a system cleaner function to improve the accuracy of EDSM-facing system commands. (@Stuntphish)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a feature where HalpyBOT will send an error message on unexpected disconnect. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Added a popup to prevent multiple submits of cases while the system is processing. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a logging system for errors to SQL. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a number of tests for HalpyBOT's code. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added in-channel responses to redirect commands. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added aioHTTP for asynchronous HTTP methods. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added several missing entries in the Help module. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added GitPython "Build Number" check to About if run in Git repo (Recommended). (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added non-ascii filtering to Fact manager. (@Rixxan)

### Changed
- [CMDR Management](https://gitlab.com/hull-seals/code/website-subsections/cmdr-management/): Updated the stored procedures used for IRC Names and CMDR Management. (@KennyRosser, @Rixxan)
- [CMDR Management](https://gitlab.com/hull-seals/code/website-subsections/cmdr-management/): Fixed several speling and grammar mistakes in the CMDR Management and IRC Names system. (@Rixxan, @KennyRosser)
- [CMDR Management](https://gitlab.com/hull-seals/code/website-subsections/cmdr-management/): Fixed an issue where an edited CMDR Platform would not properly select the platform in dropdowns. (@Rixxan)
- [Vessel Registry](https://gitlab.com/hull-seals/code/website-subsections/ship-and-carrier-management): Fixed an issue where ships marked for deletion would not actually fire weekly procedure to delete. (@KennyRosser)
- [Vessel Registry](https://gitlab.com/hull-seals/code/website-subsections/ship-and-carrier-management): Updated the stored procedures in use in Vessel Registry. (@Rixxan, @KennyRosser)
BotServ on IRC will no longer kick Voiced and Registered Seals from Repair-Requests. (@Rixxan)
- [Vessel Registry](https://gitlab.com/hull-seals/code/website-subsections/ship-and-carrier-management): Moved Ship Manager to the top level of vessel registry. (@Rixxan)
- [Vessel Registry](https://gitlab.com/hull-seals/code/website-subsections/ship-and-carrier-management): Added Datatable formatting to seal-fleet page. (@Rixxan)
- [Vessel Registry](https://gitlab.com/hull-seals/code/website-subsections/ship-and-carrier-management): Moved "Edit Ship" to the main ship registry page. (@Rixxan)
- [Vessel Registry](https://gitlab.com/hull-seals/code/website-subsections/ship-and-carrier-management): Fixed an issue where an edited ship would not properly select the ship class in dropdowns. (@Rixxan)
- [Seal Template](https://gitlab.com/hull-seals/code/seals-template): Updated all Site Subsections to use Global Theming System. (@Rixxan)
- [Seal Template](https://gitlab.com/hull-seals/code/seals-template): Updated Bootstrap and JQuery Libraries to Newer Versions. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site):Updated cache policies on most web server documents. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site):Reduced image file sizes for some files. (@Rixxan, @KennyRosser)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site):Updated security policy for webserver cookies. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site):Fixed an issue where rank badges wouldn't display on profile pages. (@Rixxan)
- [Paperwork](https://gitlab.com/hull-seals/code/website-subsections/paperwork): Fixed an issue where long planet names were not accepted by paperwork. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site):Improved the "Logout Warning" on Repair Requests / Case. (@Rixxan, Drebin)
- [Training System](https://gitlab.com/hull-seals/code/website-subsections/training-system): Fixed an issue where training emails could be sent without a set date and time. (@Rixxan)
- [Seal Template](https://gitlab.com/hull-seals/code/seals-template): Updated Modernizr for WebP support. (@Rixxan)
- [Seal Template](https://gitlab.com/hull-seals/code/seals-template): Fixed an issue preventing local swapping of fonts. (@Rixxan)
- [Seal Template](https://gitlab.com/hull-seals/code/seals-template): Changed fonts to be locally imported per [GDPR concerns](https://www.thecybersecuritytimes.com/court-orders-websites-to-stop-embedding-google-fonts-for-gdpr-violation/). (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Moved the startup script from main.py to start.py. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated all dependencies to the latest versions. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated Twitter case logic for announcement formats. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added DocStrings to various functions. (@Rik079, @Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated valid Python version to 3.8 or 3.9. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated backup facts cache for compatibility and completeness. (@Rik079, @Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Some multi-line commands will now only reply in DMs. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Completely reworked HMAC authentication for security. (@Stuntphish)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Logging can now log CLI and File levels differently. (@Stuntphish)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): The shutdown command will now log and shout out who ordered it to shut down. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated most of the project to follow PEP8 guidelines. (@Rixxan, @Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Refactored the command handler. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a bug with the announcer module if EDSM is disabled. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue with the whoami command for a particular user. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Moved the About command to the new help.py file. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a number of misspellings and grammatical errors in documentation and replies. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where the database functions were not properly declared in their init files. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Suppressed a non-issue where Pydle didn't know how to respond to certain server informational posts. (@Rik079, @Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Changed the WHOIS lookup to use database stored procedures. (@Rixxan, @KennyRosser)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where multiple given arguments in a whois lookup could break the query. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Updated Tweepy processing to adapt to newest versions of Tweepy exception handling. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a bug that could cause the force join command to fail if CMDR and Channel names were inverted. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): The logger will now rotate log files every Monday, and keep the last 12 files. (@Stuntphish)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where multiple spaces in commands would cause the bot to improperly parse arguments. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where required values were missing if a WHOIS command was used on a user not in any channels or on services. (@Rixxan, @Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where all disconnects were considered unexpected. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where language codes were not processed if in capital letters. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where a CMDR existed, but was outside of landmark range. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where coordinates would attempt to be processed if they were not all numeric. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where manual cases could be processed with bad arguments. (@Rixxan)

### Removed
- [Seal Template](https://gitlab.com/hull-seals/code/seals-template): Removed duplicate CSS and JS entries on some pages. (@Rixxan)
- [Vessel Registry](https://gitlab.com/hull-seals/code/website-subsections/ship-and-carrier-management): Removed an unused holding concept for abandoned Carrier Management System. (@Rixxan, @KennyRosser)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a few unused imports. (@Stuntphish, @Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed Facts from commands.json. (@Stuntphish)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed a few input validations that were made obsolete by the Help command. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed an unused loop variable for the API server. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed Requests library and dependencies. (@Rixxan)

## [1.4.2] - 2021-08-14
A bugfix for HalpyBOT

### Added
n/a

### Changed
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a problem where the Twitter module would not properly function. (@Rixxan).

### Removed
n/a

## [1.4.1] - 2021-08-11
A Functionality Hotfix to HalpyBOT

### Added
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a Twitter module for all incoming cases. (@Rixxan).

### Changed
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a problem with TSPING where no arguments were provided. (@Rixxan).

### Removed
n/a

## [1.4] - 2021-06-04
The fourth major feature release, with a focus on utility and removing points of failure.

### Added
- Discord: Added Case Color Images back to Case Notify - because why not (@Rixxan).
- IRC: Added censor protection against accidentally exposed NICKSERV IDENTIFY commands. (@AndrielChaoti).
- [Training System](https://gitlab.com/hull-seals/code/website-subsections/training-system): Added a "Cancellation" deletion of request and email. (@Rixxan).
- [Paperwork](https://gitlab.com/hull-seals/code/website-subsections/paperwork): Added a check to ensure that a CMDR has a valid registered CMDR Name before paperwork can be filed. (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): HalpyBOT can now check the Frontier-provided server status. (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): HalpyBOT can now set user VHOSTS with an API call. (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added entry and exit logic to the database connection. (@Rik079).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added the ability to check EDSM coordinates to in-game systems. (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): HalpyBOT now provides an approximate cardinal direction during DIST, Landmark, or DSSA checks. (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a check to ensure HalpyBOT won't force create new channels. (@Rik079).
- [Dispatch Tools](https://gitlab.com/hull-seals/code/website-subsections/dispatcher-tools): Added "Your Cases" review of a Seal's cases. (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added the ability to email or text staff members in case of issues. (@Rixxan).

### Changed
- IRC: Renamed the Discord/IRC Link Bot.
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): EDSMPing will no longer use cached results. (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue with spacing on WHOAMI commands. (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an error where some facts would not trim empty whitespace at the end of lines. (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a typo in some documentation files. (@Rixxan).
- [Training System](https://gitlab.com/hull-seals/code/website-subsections/training-system): Reworked Permission Management page, fixing several bugs. (@Rixxan).
- [Vessel Registry](https://gitlab.com/hull-seals/code/website-subsections/ship-and-carrier-management): Update Vessel Registry to one-page format, including Edit (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue with forcing EDSM to ignore the bot cache. (@Rik079).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Reworked the Fact module for speed and stability. (@Rik079).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Moved the HalpyBOT client from the main to an isolated module. (@Rik079).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Reworked the Announcer module for API usage. (@Rik079).
- [Ship Management](https://gitlab.com/hull-seals/code/website-subsections/ship-and-carrier-management): Updated to "one page" format. (@Rixxan).
- [Paperwork](https://gitlab.com/hull-seals/code/website-subsections/paperwork): Fixed an issue where paperwork could be filed without having a registered CMDR name. (@Rixxan).


### Removed
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed some duplicate Announcer messages for cases. (@Rixxan).

## [1.3.1] - 2021-03-31
A Functionality Hotfix to HalpyBOT

### Added
n/a

### Changed
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a problem with loading backup facts into memory. (@Rik079).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue preventing the DSSA carrier lookup from firing. (@Rixxan).
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue with Case Announcements. (@Rixxan).
- [CMDR Management](https://gitlab.com/hull-seals/code/website-subsections/cmdr-management/): Removed the ability for the last registered IRC name to be deleted. (@Rixxan, @KennyRosser).

### Removed
n/a

## [1.3] - 2021-03-31
The third major feature release, focusing on Quality of Life improvements for HalpyBOT and adding several requested tools and abilities.

### Added
- [Dispatch Tools](https://gitlab.com/hull-seals/code/website-subsections/dispatcher-tools): Added Kingfisher Case Review. (@Rixxan)
- [Dispatch Tools](https://gitlab.com/hull-seals/code/website-subsections/dispatcher-tools): Filing User now shows up on case listings. (@Rixxan)
- [Dispatch Tools](https://gitlab.com/hull-seals/code/website-subsections/dispatcher-tools): Case Deletion implemented for CyberSeals+. (@Rixxan, @KennyRosser)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added EDSM CMDR and System Lookups. (@Rixxan, @Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added EDSM CMDR and System Distance Calculations. (@Rixxan, @Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added WHOAMI and WHOIS Seal User Information Request commands. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added EDSM Landmark Lookup Calculations. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added In Game Year Lookup. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added Current UTC Lookup. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added Basic Bot Diagnostics Tools. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added repeating announcement if bot in OFFLINE mode. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added 5-minute cache to EDSM results. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): EDSM System Check and Landmark System Firing automatically at case generation. (@Rixxan, @Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added ability to set OFFLINE mode based on a command. (@Rixxan, @Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added Nearest DSSA Lookup. (@Rixxan)
- [Dispatch Tools](https://gitlab.com/hull-seals/code/website-subsections/dispatcher-tools): Added "My Cases" for Seals to review their own cases. (@Rixxan)


### Changed
- IRC: Changed Case Notification Messages to remove PS4 and Xbox Abbreviations. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Changed references from PS4 and XB1 specifics to generic Xbox and PlayStation. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Added a Rejoin button to Repair Requests. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Updated YOURLS to most recient version. (@Rixxan)
- [Paperwork](https://gitlab.com/hull-seals/code/website-subsections/paperwork): Fixed an issue where Dispatched repairs could be submitted without a valid Dispatcher. (@Rixxan, @KennyRosser)
- Generic: Several tools and pages received minor appearance updates. (@Rixxan)
- [CMDR Management](https://gitlab.com/hull-seals/code/website-subsections/cmdr-management/): Fixed an issue where new CMDRs could not register ships. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed Trained Seal pings and Manual Case Firing. (@Rixxan, @Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue with EDSM responses being interpreted incorrectly. (@Rik079)
- [Paperwork](https://gitlab.com/hull-seals/code/website-subsections/paperwork): Fixed an issue where paperwork could be filed without a valid CMDR registered, resulting in NULL records being created. (@Rixxan)

### Removed
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Removed "Patch Store" files. (@Rixxan)

## [1.2.3] - 2021-03-18
A Critical Security Hotfix for HalpyBOT

### Added
n/a

### Changed
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed a critical security flaw where unauthorized users could run administrative commands on HalpyBOT. (@Rik079)

### Removed
n/a

## [1.2.2] - 2021-03-06
A Functionality Hotfix to HalpyBOT

### Added
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a new "Offline" Mode for HalpyBOT Facts. (@Rik079)

### Changed
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Refreshes the database connection every time a new database-interacting command is run. (@Rik079, @KennyRosser)

### Removed
n/a

## [1.2] - 2021-03-01
The second major feature release of HalpyBOT and other Seal services

### Added
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added "Delayed Case Management" functionality. (@Rik079)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Added "Delayed Case Management" functionality. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added checks for various IRC conditions, including Oper and Channels lists. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Created an @ Command Decorator to ensure internal consistency and ease of adding new features. (@Rik079)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Added the Case Closure Status to the situation information in case review. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Implemented Direct-To-Discord Case Notifications for incoming cases. (@Rixxan)

### Changed
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Changed some internal wiring to ensure easier unit testing and ease of expansion. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where some CMDs would not respond in DMs with the bot under certain circumstances. (@Rik079)
- [Paperwork](https://gitlab.com/hull-seals/code/website-subsections/paperwork): Fixed an issue where Self Dispatched Repairs were not set in the Database properly. (@Rixxan, @KennyRosser)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Fixed an issue where the correct number of rescues a Seal participated in was not correctly displayed. (@Rixxan, @KennyRosser)
- [Paperwork](https://gitlab.com/hull-seals/code/website-subsections/paperwork): Fixed an issue where Paperwork was not requiring the Notes field to be valid. (@Rixxan, @KennyRosser)
- Wiki: Updated BookStack to the latest version. (@Rixxan)
- IRC: UnrealIRCd updated to version 5.0.8. (@Rixxan)
- IRC: m_autoban fixed a C error that could lead to a system crash. (@feliksas)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where the Permissions module would falllback to an outdated function. (@Rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Changed SetupTools dependency to v54. (@Rik079)

### Removed
- Removed the Snickers Warehouse from sublevel six.

## [1.1] - 2020-12-12
The first major feature release of HalpyBOT and other Seal services.

### Added
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added the !wssPing command to ping the "Why So Sealious" role in Seal Discord for slow responses. (@Rixxan)
- [WebhookBOT](https://gitlab.com/hull-seals/code/irc/webhookbot): Added code to interpret Paperwork's Webhook for case completion. (@Rixxan)
- [Paperwork](https://gitlab.com/hull-seals/code/website-subsections/paperwork): Added code to trigger a webhook on paperwork completion. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added an offline archive for Facts in the event the main server is unreachable. (@rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added basic bot management commands. (@rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added a command to force a user to join certain channels. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Channel list used by the case announcer can now be edited from the config file. (@rik079)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Added WebP and Fallback support. (@Rixxan)
- IRC: Linked Drill Chat in IRC with its counterpart in Discord. (@Rixxan)

### Changed
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Case Information will no longer be shouted out into Code Black. (@Rixxan)
- IRC: Fixed an issue where DCCs would cause an immediate client kick. (@Rixxan)
- IRC: Fixed an issue where excessive caps (but below threshold) could result in a client kick. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Added reference citation where one was lacking. (@rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Fixed an issue where an extra ) was output by some commands. (@rik079)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Moved NEWCASE to the top detail line of case messages, to condense notifications. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Changed Fleetcomm Discord Link to Seal Stats Link. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Changed Email Provider to Amazon Simple Email Services for NOREPLYs. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Moved "Links" footer to imagemap to save bandwidth. (@Rixxan)
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Commands are no longer case sensitive. (@Rik079)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Changed the layout of Error Pages. (@Rixxan)

### Removed
- [HalpyBOT](https://gitlab.com/hull-seals/code/irc/halpybot): Removed some unneeded regex. (@rik079)
- Journal Reader: Removed Journal Reader files from Website. Rest in peace, old friend. (@Rixxan)
- [Website](https://gitlab.com/hull-seals/code/hull-seals-main-site): Removed TLA files. (@Rixxan)

## [1.0.0] - 2020-11-20
- Initial Launch of the Hull Seals IRC Network. All records will start from this point.
